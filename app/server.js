let express = require('express');
let path = require('path');
let fs = require('fs');
let app = express();


app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, "index.html"));
});

app.use(express.static(path.join(__dirname, 'assets')));

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});

